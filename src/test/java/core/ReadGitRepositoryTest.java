package core;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class ReadGitRepositoryTest {

	private  String repoPath;
	private  GitRepository repository;
	
	@Before
	public void setup() {
		repoPath = new String("sample_repos/sample01");
		repository = new GitRepository(repoPath);
	}
	
	@Test
	public void shouldFindHead() throws Exception {
		assertThat(repository.getHeadRef()).isEqualTo("refs/heads/master");
	}
	
	//TODO: trovare da shell l'hash del commit relativo al master del repository in sample_repos/sample01
	private static String masterCommitHash = "1e7c193371b42215bc9ba653c48f58ebbb1a7aae"; 

	@Test
	public void shouldFindHash() throws Exception {
		assertThat(repository.getRefHash("refs/heads/master")).isEqualTo(masterCommitHash);
	}

	@Test
	public void shouldGetBlobContent() throws Exception {
		GitBlobObject blobObject = new GitBlobObject(repoPath,"4452771b4a695592a82313e3253f5e073e6ead8c");
		assertThat(blobObject.getType()).isEqualTo("blob");
		assertThat(blobObject.getContent()).isEqualTo("REPO DI PROVA\n=============\n\nSemplice repository Git di prova\n");		
	}

}
