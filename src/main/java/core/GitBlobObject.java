package core;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;

public class GitBlobObject {
	private String repoPath;
	private String hash;
	
	public GitBlobObject (String repoPath, String hash) {
		this.repoPath = repoPath;
		this.hash = hash;
	}
	
	public String getType() throws IOException, DataFormatException {
		String type = inflateObj (hash).split(" ")[0];
		System.out.println(type);
		return type;
	}
	
	//TODO da rivedere BENE!!
	public String getContent() {
		String objInflated = inflateObj (hash);
		int index1 = objInflated.indexOf(" ");
		objInflated = objInflated.substring(index1);
		
		String content = objInflated.substring(index1);
		System.out.println(content.length());
		System.out.println(content);
		return content;
	}
	
	public String inflateObj (String myHash) {
		String hash2 = myHash.substring(0, 2);
		String hash3 = myHash.substring(2);
		String objInflated = null;
		
		try{
		    InflaterInputStream inflaterStream = new InflaterInputStream(new FileInputStream(repoPath+"/objects/"+hash2+"/"+hash3));
		    ByteArrayOutputStream bOutStream =new ByteArrayOutputStream(512);
		    
		    int b;
		    while ((b = inflaterStream.read()) != -1) bOutStream.write(b);
		    
		    inflaterStream.close();
		    bOutStream.close();
		    objInflated = new String(bOutStream.toByteArray());
		} catch(IOException e) { System.out.println(e.getMessage()); }
        return objInflated;
	}
}
