package core;

import java.io.*;

public class GitRepository {
	private String repoPath;
	
	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}
	
	public String getHeadRef () throws IOException {
		String headRef = readString("\\HEAD").split(":")[1].trim();
		
		return headRef;
	}
	
	public String getRefHash (String path) throws IOException {
		String hash = readString("\\"+path);
		
		return hash;
	}
	
	private String readString (String filename) throws IOException {
		String tmp;
		FileReader f = new FileReader(repoPath+filename);
		BufferedReader in = new BufferedReader(f);
		
		tmp = in.readLine();
		
		in.close();
		System.out.println(tmp);
		
		return tmp;
	}
	
}
